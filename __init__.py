__author__ = 'Maddagaska'
__versioninfo__ = (0, 4, 0)
__version__ = '.'.join(map(str, __versioninfo__))
import sys
import maddautils.validate as validate


def request_if_not_set(user_input='',
                       source=sys.stdin,
                       dest=sys.stdout,
                       prompt='Enter data: ',
                       valid_types=('string',)):
    """
        Prompt for data if it is not set.

        Defaults to an input source of sys.stdin
        Defaults an output destination of sys.stdout

        Takes a list of valid types which are validated using the validation
        library in maddautils.

        Defaults to accepting any non zero length string as input.

        While an empty string can be accepted as valid input, it will still
        prompt if an empty string is initially supplied. However, it will then
        accept the empty string.

        Will return the set value.
    """
    user_input = user_input.rstrip()
    initial_input = True

    while not validate.valid(user_input, valid_types) or initial_input:
        # We return if the right input was supplied and it wasn't empty
        # on the first pass
        if validate.valid(user_input, valid_types):
            # Make sure an empty string isn't accepted on the first pass
            if initial_input:
                if user_input != '':
                    break
                else:
                    # Don't accept an empty string on the first pass
                    pass
            else:
                break

        dest.write(prompt)
        dest.flush()

        user_input = source.readline().rstrip()

        initial_input = False

        if not validate.valid(user_input, valid_types):
            dest.write('Invalid input.\n')
            dest.flush()

    return user_input


def prompt_for_confirmation(source=sys.stdin,
                            dest=sys.stdout,
                            default='-',
                            prompt='Enter'):
    """
        Prompt for confirmation from a user.

        Defaults to an input source of sys.stdin
        Defaults an output destination of sys.stdout

        If a default option is set, it will be used if the user responds with
        blank input.

        Prompt can be customised, but will still list options.

        Returns True for yes and False for no.
    """
    valid_choices = {'yes': True, 'no': False}
    output_choices = []
    for choice in valid_choices.keys():
        if choice.startswith(default):
            choice = choice.capitalize()
        output_choices.append(choice)

    # Guido forgive me, for I have while Trued.
    while True:
        dest.write('%s [%s]: ' % (prompt, '/'.join(output_choices)))
        dest.flush()

        response = source.readline().rstrip().lower()

        if response == '':
            if default is not '-':
                response = default

        if response != '':
            for choice in valid_choices.keys():
                if choice.startswith(response):
                    return valid_choices[choice]

        # No valid choice given, complain
        dest.write('Please enter a valid response.\n')


def choose_item_from_list(items,
                          source=sys.stdin,
                          dest=sys.stdout,
                          default='-',
                          prompt='Please select an item:'):
    # No item can be selected if there are no items
    if len(items) == 0:
        return -1

    # Use the default if we can- otherwise, make sure it's safe
    if default != '-' and (default in items or default == 'None'):
        if default == 'None':
            default = -1
        else:
            default = items.index(default)
    elif isinstance(default, int) and default in range(-1, len(items)):
        # The default is already an index in the list, or it is none
        # We don't need to do anything
        pass
    else:
        # If the default has been incorrectly set, or if it has not been set,
        # set it to nothing
        default = None

    # Ask the user to make a selection
    selection = None
    while selection is None:
        selection = default

        # Output the header
        dest.write('%s\n' % prompt)
        dest.write('0. None')
        if default == -1:
            # This is the default option
            dest.write(' (default)\n')
        else:
            dest.write('\n')

        # Output all of the items
        for item in range(0, len(items)):
            dest.write('%s. %s' % (item + 1, items[item],))
            if int(item) == default:
                # This is the default option
                dest.write(' (default)\n')
            else:
                dest.write('\n')

        dest.flush()

        response = source.readline().rstrip()

        if len(response) == 0:
            # No response given, prompt again
            # Otherwise it causes a problem with 'startswith' later
            continue

        try:
            # If the input is an integer we treat it as a selection
            # We don't try to match it to the text of a list item
            response = int(response)
            response -= 1
            if response in range(-1, len(items)):
                selection = response
            else:
                # Give helpful feedback
                response += 1
                dest.write('\n')
                dest.write('I do not have an item number %s.\n' % response)
                dest.write('\n')
                dest.flush()

        except ValueError:
            # It wasn't an integer. Did it match any items in the list?
            candidates = []
            for item in ['None'] + items:
                if item.startswith(response):
                    candidates.append(item)

            # If the selection is unambiguous, accept it as the choice
            if len(candidates) == 1:
                if 'None'.startswith(response):
                    selection = -1
                else:
                    for item in items:
                        if item.startswith(response):
                            selection = items.index(item)
            elif len(candidates) > 1:
                # The response was ambiguous, it could match multiple items
                dest.write('\n')
                dest.write('%s matches multiple items. ' % response)
                dest.write('Please be more specific.\n')
                dest.write('\n')
                dest.flush()
            else:
                # The named item wasn't in our list, give useful feedback
                dest.write('\n')
                dest.write('%s is not an option.\n' % response)
                dest.write('\n')
                dest.flush()

    return selection


def semicolon_split(input_data):
    """
        Split a string on semicolons.
        Allows escaping of semicolons with backslashes.
    """
    if '\\;' not in input_data:
        # If there are no escape characters followed by semicolons in the
        # input then we don't need to do anything particularly complicated
        # We can just split and replace any sequences of multiple backslashes
        # with single backslashes
        results = input_data.split(';')
        results = [item.replace('\\\\','\\') for item in results]
    else:
        # There may exist escaped semicolons in the input
        results = []
        current_result = ''
        decode = ''
        for character in input_data:
            if character == '\\':
                if decode == '\\':
                    # If the current and previous characters are both
                    # backslashes, replace them with one
                    current_result += '\\'
                    decode = ''
                else:
                    # If the current character is a backslash, the next one
                    # may need to be escaped
                    decode = '\\'
            elif character == ';':
                if decode == '\\':
                    # If the previous character was a non-escaped backslash,
                    # the current semicolon must be treated as being a literal
                    # semicolon, not a split marker
                    current_result += ';'
                    decode = ''
                else:
                    # If this was not an escaped semicolon, split on it
                    results.append(current_result)
                    # And get ready for the next component
                    current_result = ''
                    decode = ''
            else:
                # To give the same behaviour as the simple split above
                # We must keep any single backslashes
                if decode == '\\':
                    current_result += decode
                    decode = ''
                current_result += character

        # Catch the last result too
        results.append(current_result)

    # Remove leading and trailing whitespace.
    results = [item.lstrip().rstrip() for item in results]

    return results


class stdout_tester:
    """
        Stub for testing classes which expect to be seeing a sys.stdout-alike
        object.

        Behaves superficially like a stream object in very basic ways.
    """
    def __init__(self):
        self.data = []

    def write(self, string):
        self.data.append(string)
        return len(string)

    def read(self):
        return ''.join(self.data)

    def close(self):
        pass

    def flush(self):
        pass
