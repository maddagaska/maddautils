from maddautils import choose_item_from_list
from maddautils import stdout_tester
import io


def test_no_items_provided():
    input_data = io.StringIO('\n')

    expected_output = ''

    expected_return = -1

    default = '-'

    items = []

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_no_item():
    input_data = io.StringIO('0\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n'
    ]

    expected_return = -1

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_no_item_by_name():
    input_data = io.StringIO('None\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n'
    ]

    expected_return = -1

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_item():
    input_data = io.StringIO('2\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n'
    ]

    expected_return = 1

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_item_by_name():
    input_data = io.StringIO('Tom\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n'
    ]

    expected_return = 0

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_out_of_bounds():
    input_data = io.StringIO('4\n3\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '\n',
        'I do not have an item number 4.\n',
        '\n',
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n'
    ]

    expected_return = 2

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_by_partial_name():
    input_data = io.StringIO('T\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n'
    ]

    expected_return = 0

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry',
        'Harold'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_by_partial_name_ambiguous():
    input_data = io.StringIO('Har\nHarr\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n',
        '\n',
        'Har matches multiple items. Please be more specific.\n',
        '\n',
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n'
    ]

    expected_return = 2

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry',
        'Harold'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_choose_by_name_missing():
    input_data = io.StringIO('Robert\nTom\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n',
        '\n',
        'Robert is not an option.\n',
        '\n',
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n'
    ]

    expected_return = 0

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry',
        'Harold'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_no_input():
    input_data = io.StringIO('\nTom\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n',
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry\n',
        '4. Harold\n'
    ]

    expected_return = 0

    default = '-'

    items = [
        'Tom',
        'Dick',
        'Harry',
        'Harold'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_no_input_with_default():
    input_data = io.StringIO('\n')

    expected_output = [
        'Please select an item:\n',
        '0. None\n',
        '1. Tom\n',
        '2. Dick\n',
        '3. Harry (default)\n',
        '4. Harold\n'
    ]

    expected_return = 2

    default = 'Harry'

    items = [
        'Tom',
        'Dick',
        'Harry',
        'Harold'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_alternative_prompt():
    input_data = io.StringIO('0\n')

    expected_output = [
        'Choose a fruit:\n',
        '0. None\n',
        '1. banana\n',
        '2. pear\n',
        '3. apple\n'
    ]

    default = '-'

    expected_return = -1

    prompt = 'Choose a fruit:'

    items = [
        'banana',
        'pear',
        'apple'
    ]

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        items,
        default,
        prompt
    )

    assert test_return['right_output']
    assert test_return['right_return']


def wrapper(input_data,
            expected_output,
            expected_result,
            items,
            default='-',
            prompt='Please select an item:'):
    if isinstance(expected_output, list):
        expected_output = ''.join(expected_output)

    output_data = stdout_tester()

    result = choose_item_from_list(
        items=items,
        source=input_data,
        dest=output_data,
        default=default,
        prompt=prompt
    )

    if output_data.read() != expected_output:
        print('Expected output:')
        print(expected_output)
        print()
        print('Actual output:')
        print(output_data.read())

    if expected_result != result:
        print('Expected return code: %s' % expected_result)
        print('Actual return code: %s' % result)

    return {
        'right_output': output_data.read() == expected_output,
        'right_return': expected_result == result
    }
