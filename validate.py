# A set of data validation routines


def valid(data, check_types):
    """
        Is the data provided valid?
        Can be provided with one type to check as a string,
        or with multiple as a tuple or list.

        Will return true for a tuple/list if the data matches any one of the
        supplied types.

        An exception will be raised if we don't know the supplied type.
    """
    data_types = {
        'string': valid_string,
        'integer_string': valid_integer_string,
        'empty_string': valid_empty_string,
        'isbn': valid_isbn,
        'isbn_10': valid_isbn_10,
        'isbn10': valid_isbn_10,
        'isbn_13': valid_isbn_13,
        'isbn13': valid_isbn_13
    }

    # If we can't determine the validity, an exception is probably the right
    # response to give, so we won't check if data_type is in data_types

    if isinstance(check_types, tuple) or isinstance(check_types, list):
        for check_type in check_types:
            if data_types[check_type](data):
                return True

    elif isinstance(check_types, str):
        return data_types[check_type](data)

    # It was not valid as it failed to be validated above (fall through
    # primarily for tuple or list input
    return False


def valid_string(data):
    """
        Is this data a string of non-zero length?
        We have a separate zero length check.
    """
    return isinstance(data, str) and len(data) > 0


def valid_integer_string(data):
    """
        Is this data a string containing an integer?
    """
    if valid_string(data):
        try:
            int(data)
            return True
        except:
            return False
    else:
        return False


def valid_empty_string(data):
    """
        Is this data an empty string?
    """
    return data == ''


def valid_isbn(data):
    """
        Is this a valid ISBN (10 or 13)?
    """
    data = data.replace('-', '')
    if len(data) == 10:
        return valid_isbn_10(data)
    elif len(data) == 13:
        return valid_isbn_13(data)
    else:
        return False


def valid_isbn_10(data):
    """
        Is this a valid ISBN10?
    """
    data = data.replace('-', '')
    if len(data) != 10:
        return False

    if not valid_integer_string(data):
        return False

    # Now that we know it has a hope of being valid, let's verify it
    total = 0

    for digit in range(0, 10):
        multiplier = 10 - digit
        value = int(data[digit])
        total += multiplier * value

    if total % 11 == 0:
        return True
    else:
        return False


def valid_isbn_13(data):
    """
        Is this a valid ISBN13?
    """
    data = data.replace('-', '')
    if len(data) != 13:
        return False

    if not valid_integer_string(data):
        return False

    # Now that we know it has a hope of being valid, let's verify it
    total = 0

    multipliers = (1, 3)

    for digit in range(0, 12):
        multiplier = multipliers[digit % 2]
        value = int(data[digit])
        total += multiplier * value

    check_calculated = 10 - (total % 10) % 10

    if check_calculated == int(data[12]):
        return True
    else:
        return False
