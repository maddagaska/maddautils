from maddautils import prompt_for_confirmation
from maddautils import stdout_tester
import io


def test_just_say_yes():
    input_data = io.StringIO('yes\n')

    expected_output = 'Enter [yes/no]: '

    expected_return = True

    default = '-'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_just_say_no():
    input_data = io.StringIO('no\n')

    expected_output = 'Enter [yes/no]: '

    expected_return = False

    default = '-'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_default_yes():
    input_data = io.StringIO('\n')

    expected_output = 'Enter [Yes/no]: '

    expected_return = True

    default = 'y'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_default_no():
    input_data = io.StringIO('\n')

    expected_output = 'Enter [yes/No]: '

    expected_return = False

    default = 'n'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_curious_user():
    input_data = io.StringIO('Test\nHello\nYes\n')

    expected_output = [
        'Enter [yes/no]: ',
        'Please enter a valid response.\n',
        'Enter [yes/no]: ',
        'Please enter a valid response.\n',
        'Enter [yes/no]: '
    ]

    expected_return = True

    default = '-'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_curious_user_with_default():
    input_data = io.StringIO('Test\nHello\n\n')

    expected_output = [
        'Enter [yes/No]: ',
        'Please enter a valid response.\n',
        'Enter [yes/No]: ',
        'Please enter a valid response.\n',
        'Enter [yes/No]: '
    ]

    expected_return = False

    default = 'n'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default
    )

    assert test_return['right_output']
    assert test_return['right_return']


def test_alternative_prompt():
    input_data = io.StringIO('Maybe\n\nyes\n')

    expected_output = [
        'Do you wish to continue? [yes/no]: ',
        'Please enter a valid response.\n',
        'Do you wish to continue? [yes/no]: ',
        'Please enter a valid response.\n',
        'Do you wish to continue? [yes/no]: '
    ]

    default = '-'

    expected_return = True

    prompt = 'Do you wish to continue?'

    test_return = wrapper(
        input_data,
        expected_output,
        expected_return,
        default,
        prompt
    )

    assert test_return['right_output']
    assert test_return['right_return']


def wrapper(input_data,
            expected_output,
            expected_result,
            default='-',
            prompt='Enter'):
    if isinstance(expected_output, list):
        expected_output = ''.join(expected_output)

    output_data = stdout_tester()

    result = prompt_for_confirmation(
        source=input_data,
        dest=output_data,
        default=default,
        prompt=prompt
    )

    if output_data.read() != expected_output:
        print('Expected output:')
        print(expected_output)
        print()
        print('Actual output:')
        print(output_data.read())

    if expected_result != result:
        print('Expected return code: %s' % expected_result)
        print('Actual return code: %s' % result)

    return {
        'right_output': output_data.read() == expected_output,
        'right_return': expected_result == result
    }
