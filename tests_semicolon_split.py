from maddautils import semicolon_split


def test_no_splits():
    input_data = 'this is a test'

    expected_result = ['this is a test']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_one_split():
    input_data = 'this is a test;so is this'

    expected_result = ['this is a test', 'so is this']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_one_split_trailing_space():
    input_data = 'this is a test ;so is this'

    expected_result = ['this is a test', 'so is this']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_one_split_leading_space():
    input_data = 'this is a test; so is this'

    expected_result = ['this is a test', 'so is this']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_multiple_splits():
    input_data = 'this is a test; so is this; and this too'

    expected_result = ['this is a test', 'so is this', 'and this too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_escaped_semicolon():
    input_data = 'this is a test; this is a \\; test too'

    expected_result = ['this is a test', 'this is a ; test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_string_ends_in_literal_slash():
    input_data = 'this is a test\\\\; this is a test too'

    expected_result = ['this is a test\\', 'this is a test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_string_contains_slash_followed_by_escaped_semicolon():
    input_data = 'this is a test; this is a \\\\\\; test too'

    expected_result = ['this is a test', 'this is a \\; test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_string_has_unescaped_backslash():
    input_data = 'this \\is a test; this is a test too'

    expected_result = ['this \\is a test', 'this is a test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_string_unescaped_backslash_and_end_with_literal_backslash():
    input_data = 'this\\ is a test\\\\; this is a test too'

    expected_result = ['this\\ is a test\\', 'this is a test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def test_string_unescaped_backslash_and_escaped_semicolon():
    input_data = 'this is a test; this is \\;\\ a test too'

    expected_result = ['this is a test', 'this is ;\\ a test too']

    test_return = wrapper(
        input_data,
        expected_result
    )

    assert test_return['right_return']


def wrapper(input_data,
            expected_result):
    result = semicolon_split(
        input_data
    )

    if expected_result != result:
        print('Expected return was: %s' % expected_result)
        print('Actual return was: %s' % result)

    return {
        'right_return': expected_result == result
    }
